const express = require('express');
const cors = require("cors");
const users = require('./app/users');
const tasks = require('./app/tasks');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/users', users);
app.use('/tasks', tasks);

const run = async () => {
  await mongoose.connect('mongodb://localhost/tasks', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    console.log('Mongoose disconnected');
    callback();
  });
}
run().catch(console.error);