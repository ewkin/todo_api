const express = require('express');
const auth = require("../middleware/auth");
const Task = require('../models/Task');

const router = express.Router();

router.post('/', auth, async (req, res) => {
  try {
    const taskData = req.body;
    if (taskData.user != req.user._id) {
      return res.status(403).send({error: 'Forbidden'});
    }
    const task = new Task(taskData);
    await task.save();
    return res.send(task);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const tasks = await Task.find({user: req.user._id});
    res.send(tasks);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/:id', auth, async (req, res) => {
  try {
    const conditions = {_id: req.params.id, user: req.user._id};
    const options = {runValidators: true, new: true};
    const taskData = req.body;
    delete taskData.user;
    Task.findOneAndUpdate(conditions, taskData, options, (err, todo) => {
      if (err || !todo) return res.status(403).send({error: 'Forbidden'});
      return res.send(todo);
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const conditions = {_id: req.params.id, user: req.user._id};
    Task.findOneAndDelete(conditions,  (err, todo) => {
      if (err || !todo) return res.status(403).send({error: 'Forbidden'});
      return res.send('Deleted');
    });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;