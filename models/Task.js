const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,

  },
  description: String,
  status: {
    type: String,
    enum: ['new', 'in_progress', 'complete'],
    default: 'new'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true

  }
});

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;